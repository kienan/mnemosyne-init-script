# Mnemosyne Init Script

A SysV init script for [Mnemosyne][1]

## License and Copyright

Based on [init-script-template][2] by Felix H. Dahlke, with
further modifications by Kienan Stewart.

Distributed under the MIT License, see LICENSE for the exact terms.

## Installation

1. Clone or unpack a copy of the files into the folder /etc/mnemosyne
1. Create a symbolic link or copy the config file into /etc/default/mnemosyne

    ln --symbolic /etc/mnemosyne/config /etc/default/mnemosyne

1. Create a symbolic link or copy the init script into /etc/init.d/mnemosyne

    ln --symbolic /etc/mnemosyne/mnemosyne /etc/init.d/mnemosyne

1. Ensure /etc/init.d/mnemosyne (or the file it points to) is executable

    chmod +x /etc/mnemosyne/mnemosyne

1. Configure /etc/default/mnemosyne as needed
1. Run: update-rc.d mnemosyne defaults

## Configuration

Set the MNEMOSYNE\_DATA\_DIR to your data directory (which contains default.db
and config.db). The init script assumes a mnemosyne user exists and has a
home directory at /home/mnemosyne. The data directory defaults to
/home/mnemosyne/mnemosyne-data.

Change any settings necessary, the defaults are noted in comments in the config file.

The default run-levels:

* Start on: 2 3 4 5
* Stop on:  0 1 6

## Notes

The pid directory /var/run/mnemosyne and log directory /var/log/mnemosyne will
be created and chowned to the user set if they don't exist. If you change the
user, re-chown the directories and files in them.

[1]: http://mnemosyne-proj.org "The Mnemosyne Project"
[2]: https://github.com/fhd/init-script-template "Init-script Template by Flex H. Dahlke on GitHub"
